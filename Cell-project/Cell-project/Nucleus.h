#pragma once
using std::string;


class Gene
{
public:
	//methods
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	
		//getters
		int get_start() const;
		int get_end() const;
		bool is_on_complementary_dna_strand() const;

		//setters
		void setStart(const unsigned int newStart);
		void setEnd(const unsigned int newEnd);
	

private:
	unsigned int _start; //the strat index of the gene
	unsigned int _end; //the end index of the gene
	bool _on_complementary_dna_strand; //if the gene is in the complementary dna strand(true/false)

};


class Nucleus
{
public:
	//methods
	void init(const string dna_sequence);
	string get_RNA_transcript(const Gene& gene) const;
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const string& codon) const;


private:
	string _strand_DNA; //the strand DNA
	string _complementary_DNA_strand; //the complementary strand DNA

	//methods
	string Build_RNA(string DNA_seq, unsigned int start, unsigned int end) const;
};
