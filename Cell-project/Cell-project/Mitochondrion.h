#pragma once
class Mitochondrion
{
public:
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;

private:
	unsigned int _level_glocuse;
	bool _receptor_glocuse_has;

};

