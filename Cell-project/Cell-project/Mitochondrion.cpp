#include <iostream>
#include <string>
#include "AminoAcid.h"
#include "Protein.h"
#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

#define MIN_GLU_LVL 50
#define MAX_ARR 7

/*
	function will set class Mitochondrion for work
	input: none
	output: none
*/
void Mitochondrion::init()
{
	this->_level_glocuse = 0;
	this->_receptor_glocuse_has = false;
}


/*
	function will check if the protein given is like the protein you want
	input: protein- the protein list
	output: none
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	int check = 1;
	int count = 0;
	string val = "";
	Protein curr = protein;
	
	AminoAcid arr[MAX_ARR] = {ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END};
	while ((curr.get_first()->get_next()) && check != 0)
	{
		if (curr.get_first()->get_data() == arr[count])
		{
			curr.set_first(curr.get_first()->get_next());
			count++;
		}
		else
		{
			check = 0;
		}
	}

	if(check)
	{
		this->_receptor_glocuse_has = true;
	}
}


/*
	function will set glucose level
	input: glocuse_units- the glucose level to set
	output: none
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_level_glocuse = glocuse_units;
}


/*
	function will check if the Mitochondrion can produce ATP
	input: none
	output: true or false if the Mitochondrion can produce ATP
*/
bool Mitochondrion::produceATP() const
{
	bool flag = false;

	if (this->_level_glocuse >= MIN_GLU_LVL && this->_receptor_glocuse_has)
	{
		flag = true;
	}

	return flag;
}