#pragma once
#include "Protein.h"
#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"


class Cell
{
public:
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();


private:
	Nucleus _nucleus;
	Ribosome ribosome;
	Mitochondrion _mitochondrion;
	Gene _gene_receptor_glocus;
	unsigned int _units_atp;
};

