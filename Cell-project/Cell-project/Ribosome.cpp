#include <iostream>
#include <string>
#include "AminoAcid.h"
#include "Protein.h"
#include "Ribosome.h"


using std::cout;
using std::cin;
using std::endl;
using std::string;

#define CODON_MAX 3

/*
	function willcreate a protein by the RNA
	input: RNA_transcript = the RNA sequence
	output: the protein linkedList
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	int i = 0;
	int pos = 0;

	Protein* pro = new Protein;

	string nuc = RNA_transcript;
	string nuc_amino = RNA_transcript;

	AminoAcid amino;

	pro->init();

	for (i = 0; (i < (nuc.length() / CODON_MAX) - 1) && pro != nullptr; i++)
	{
		nuc_amino = nuc.string::substr(pos, CODON_MAX);

		amino = get_amino_acid(nuc_amino);

		if (amino == UNKNOWN)
		{
			pro->clear();
			pro = nullptr;
		}
		else
		{
			pro->add(amino);
		}
		nuc = nuc.string::substr(pos);
		pos += CODON_MAX;
		
	}
	return pro;
}