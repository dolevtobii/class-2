#include "Cell.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

#define MIN_GLU_LVL 50
#define NEXT_UNIT_VALUE 100

void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_gene_receptor_glocus = glucose_receptor_gene;

	this->_mitochondrion.init();
}


bool Cell::get_ATP()
{
	bool result = false;
	string RnaTranscript = this->_nucleus.get_RNA_transcript(this->_gene_receptor_glocus);

	Protein* pro = this->ribosome.create_protein(RnaTranscript);

	if(pro == nullptr)
	{
		std::cerr << "can not creat protein!";
		_exit(1);
	}

	this->_mitochondrion.insert_glucose_receptor(*pro);
	this->_mitochondrion.set_glucose(MIN_GLU_LVL);

	if(this->_mitochondrion.produceATP())
	{
		this->_units_atp = NEXT_UNIT_VALUE;
		result = true;
	}

	return result;
}