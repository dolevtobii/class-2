#include <iostream>
#include <string>
#include "Nucleus.h"


using std::cout;
using std::cin;
using std::endl;
using std::string;



//-----------------------------------------------------------------------------------------
//Nucleus

/*
	function will set Nucleus class for work
	input: dna_sequence- the dna sequence
	output: none
*/
void Nucleus::init(const string dna_sequence)
{
	int i = 0;
	int dna_length = dna_sequence.length();

	this->_strand_DNA = dna_sequence;
	this->_complementary_DNA_strand = "";

	for(i = 0; i < dna_length; i++)
	{
		if (this->_strand_DNA[i] == 'A')
		{
			this->_complementary_DNA_strand += 'T';
		}
		else if (this->_strand_DNA[i] == 'T')
		{
			this->_complementary_DNA_strand += 'A';
		}
		else if (this->_strand_DNA[i] == 'G')
		{
			this->_complementary_DNA_strand += 'C';
		}
		else if (this->_strand_DNA[i] == 'C')
		{
			this->_complementary_DNA_strand += 'G';
		}
		else 
		{
			std::cerr << "The D.N.A sequence includes undefineded letters(not A, T, G, C)";
			_exit(1);
		}
	}
}


/*
	function will return the RNA sequence
	input: gene- reference to the gene 
	output: the RNA sequence
*/
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string RNA = "";

	if (gene.is_on_complementary_dna_strand())
	{
		RNA = this->Build_RNA(this->_complementary_DNA_strand, gene.get_start(), gene.get_end());
	}
	else
	{
		RNA = this->Build_RNA(this->_strand_DNA, gene.get_start(), gene.get_end());
	}

	
	return RNA;
}


/*
	function will build the RNA sequence
	input: DNA_seq- reference to the gene
	output: the RNA sequence
*/
string Nucleus::Build_RNA(string DNA_sequence, unsigned int start, unsigned int end) const
{
	int i = 0;
	string RNA = "";

	for (i = start; i <= end; i++)
	{
		if (DNA_sequence[i] == 'T')
		{
			RNA += 'U';
		}
		else
		{
			RNA += DNA_sequence[i];
		}
	}

	return RNA;
}


/*
	function will return the reversed DNA sequence
	input: none
	output: the reversed string
*/
string Nucleus::get_reversed_DNA_strand() const
{
	int i = 0;
	int dna_length = this->_strand_DNA.length();
	string result = "";

	for (i = dna_length; i >= 0; i--)
	{
		result += this->_strand_DNA[i];
	}

	return result;
}


/*
	function will count how many times the given codon has appear in the strand
	input: codon- the 3 nucleotides(codon)
	output: how many time the codon has appear in the strand
*/
unsigned int Nucleus::get_num_of_codon_appearances(const string& codon) const
{
	int DNA_leangh = this->_strand_DNA.length();
	int result = 0;
	int position = 0;

	for (position = 0; (position = this->_strand_DNA.find(codon, position)) != std::string::npos; position++)
	{
		result++;
	}

	return result;
}

//-----------------------------------------------------------------------------------------
//Gene


/*
	function will set Gene class for work
	input: start- the start index of the Gene
		   end- the end index of the Gene
		   on_complementary_dna_strand- true or false if the gene is on the strand
	output: none
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;

}


//Gene getters

/*
	function will return the start index of the gene
	input: none
	output: the start index of the gene
*/
int Gene::get_start() const
{
	return this->_start;
}


/*
	function will return the end index of the gene
	input: none
	output: the end index of the gene
*/
int Gene::get_end() const
{
	return this->_end;
}


/*
	function will return if the gene is on the strand
	input: none
	output: true or false if the gen is on the strand
*/
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}


//Gene setters 

/*
	function will set the start index of the gene
	input: newStart- the value to insert to the start variable
	output: none
*/
void Gene::setStart(const unsigned int newStart)
{
	this->_start = newStart;
}


/*
	function will set the end index of the gene
	input: newEnd- the value to insert to the End variable
	output: none
*/
void Gene::setEnd(const unsigned int newEnd)
{
	this->_start = newEnd;
}